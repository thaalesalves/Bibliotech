﻿//////////////////////////////////////////
//     PROJETO DE AMBIENTES VISUAIS     //
//////////////////////////////////////////
//              BIBLIOTECH              //
//    SISTEMA INTEGRADO BIBLIOTECÁRIO   //
//////////////////////////////////////////
//  THALES ALVES PEREIRA (11141101271)  //
//  GILBERTO OLIVEIRA ()                //
//  LINCON NASCIMENTO ()                //
//////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;

namespace sistemaBiblioteca {
    class mainDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

        /* Declaração de Variáveis */
        static string connString = null;
        string criacaoUsuario =
            "CREATE TABLE Usuario(" +  // Criação da tabela de dados do usuário
                "id SERIAL PRIMARY KEY," +  // Campo de ID do usuário com autoincremento
                "nome VARCHAR NOT NULL," + // Campo de nome do usuário
                "sobrenome VARCHAR NOT NULL," + // Campo de sobrenome do usuário
                "nascimento VARCHAR NOT NULL," + // Campo da data de nascimento do usuário
                "sexo VARCHAR NOT NULL," + // Campo do sexo do usuário
                "cpf VARCHAR NOT NULL," + // Campo do CPF do usuário
                "rg VARCHAR NOT NULL," + // Campo do RG do usuário
                "endereco VARCHAR NOT NULL," + // Campo do endereço do usuário
                "numero INT NOT NULL," + // Campo do número da casa do usuário
                "cep VARCHAR NOT NULL," + // Campo do CEP da rua do usuário
                "cidade VARCHAR NOT NULL," + // Campo da cidade na qual o usuário mora
                "uf VARCHAR NOT NULL," + // Campo do estado no qual a cidade do usuário fica
                "fixo VARCHAR," + // Campo do telefone fixo do usuário
                "celular VARCHAR," + // Campo do celular do usuário
                "email VARCHAR," + // Campo do e-mail do usuário
                "website VARCHAR" + // Campo do website pessoal do usuário
            "); ";

        string criacaoLivro =
            "CREATE TABLE Livro(" + // Criação da tabela de dados dos livros
                "id SERIAL PRIMARY KEY," + // Campo de ID do livro com autoincremento
                "titulo VARCHAR NOT NULL," + // Campo de título do livro
                "genero VARCHAR NOT NULL," + // Campo de gênero do livro
                "editora VARCHAR NOT NULL," + // Campo de editora do livro
                "ano INT NOT NULL," + // Campo com o ano de publicação do livro 
                "autor VARCHAR NOT NULL," + // Campo com o nome do autor
                "ultimoNome VARCHAR NOT NULL" + // Campo com o sobrenome do autor
            "); ";

        string criacaoEditora = 
            "CREATE TABLE Editora(" + // Criação da tabela de dados da editora
                "id SERIAL PRIMARY KEY," + // Campo de ID da editora com autoincremento
                "nome VARCHAR NOT NULL," + // Campo de nome da editora
                "cnpj VARCHAR NOT NULL," + // Campo do CNPJ da editora
                "endereco VARCHAR NOT NULL" + // Campo com endereço da editora
            "); ";

        string criacaoEstoque =
            "CREATE TABLE Estoque(" + // Criação da tabela de dados de estocagem
                "id_livro SERIAL PRIMARY KEY REFERENCES livro(id) ON DELETE CASCADE ON UPDATE CASCADE," + // Campo inteiro de ID que contém o ID do livro no estoque
                "qtd INT NOT NULL" + // Campo inteiro que indica a quantidade de instâncias do item acima
            "); ";

        string criacaoAuxAluga = 
            "CREATE TABLE auxAluga(" + // Criação da tabela auxiliar que liga o usuário ao livro para aluguel
                "id SERIAL PRIMARY KEY," + // Campo de ID do aluguel com autoincremento
                "id_livro INT REFERENCES livro(id) ON DELETE CASCADE ON UPDATE CASCADE," + // Campo inteiro que contém o ID do livro alugado
                "id_usuario INT REFERENCES usuario(id) ON DELETE CASCADE ON UPDATE CASCADE," + // Campo inteiro que contém do ID do usuário que alugou o livro
                "data_aluguel DATE," + // Campo que contém a data do aluguel do livro
                "data_devol DATE" + // Campo que contém a data que o livro deve ser devolvido
            "); ";
        
        string criacaoAuxFornece = 
            "CREATE TABLE auxFornece(" + // Criação da tabela auxiliar que liga o livro à editora para fornecimento
                "id SERIAL PRIMARY KEY," + // Campo de ID do pedido com autoincremento
                "id_editora INT REFERENCES editora(id) ON DELETE CASCADE ON UPDATE CASCADE," + // Campo inteiro que contém do ID da editora fornecedora
                "id_livro INT REFERENCES livro(id) ON DELETE CASCADE ON UPDATE CASCADE," + // Campo inteiro que contém o ID do livro pedido
                "data_forn DATE" + // Campo que contém a data da compra de materiais
            "); ";

        /* Construtor */
        public mainDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            ); 
        }    

        /* Override de Banco de Dados (emergência) */
        public void criaTabelas() {
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format(criacaoUsuario + criacaoLivro + criacaoEditora + criacaoEstoque + criacaoAuxAluga + criacaoAuxFornece);
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection))
                    {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }

        public void deletaTabelas() {
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString))
                {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format("DROP SCHEMA public CASCADE; CREATE SCHEMA public;");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }

        public void enviaComandos(string comandos) {
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format(comandos);
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        } 

        /* Métodos */
        public void AtualizarRegistro(string tabela, string dado, string dado2, string coluna, string coluna2) { // Atualiza registro a partir de condição
            try { 
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {  
                    pgsqlConnection.Open(); 
                    string cmdAtualiza = String.Format("UPDATE " + tabela + " SET " + coluna + "= '" + dado + "' WHERE " + coluna2 + " = " + dado2); 
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdAtualiza, pgsqlConnection)) { 
                        pgsqlcommand.ExecuteNonQuery(); 
                    } 
                } 
            } 
            
            catch (NpgsqlException ex) { 
                throw ex; 
            } 
            
            catch (Exception ex) {                
                throw ex; 
            } 
            
            finally { 
                pgsqlConnection.Close(); 
            } 
        }

        public void DeletarRegistro(string dado, string coluna, string tabela) {  // Deleta registros
            try { 
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) { 
                    pgsqlConnection.Open(); 
                    string cmdDeletar = String.Format("DELETE FROM " + tabela + " WHERE " + coluna + " = " + dado); 
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdDeletar, pgsqlConnection)) { 
                        pgsqlcommand.ExecuteNonQuery(); 
                    } 
                } 
            } 
            
            catch (NpgsqlException ex) { 
                throw ex; 
            } 
            
            catch (Exception ex) {
                throw ex; 
            } 
            
            finally { 
                pgsqlConnection.Close(); 
            } 
        }
    }
}
