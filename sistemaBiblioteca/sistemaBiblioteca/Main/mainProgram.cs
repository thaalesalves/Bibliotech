﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace sistemaBiblioteca {
    public partial class frmMainProgram : Form {
        /* Instanciação de Objetos */
        mainDAO dao = new mainDAO(); 
        cadastroCliente cadastroCliente = new cadastroCliente();
        consultaCliente consultaCliente = new consultaCliente();
        cadastroLivro cadastroLivro = new cadastroLivro();
        consultaLivro consultaLivro = new consultaLivro();
        alugaLivro alugaLivro = new alugaLivro();
        pedidoLivro pedidoLivro = new pedidoLivro();
        
        public frmMainProgram() {
            InitializeComponent();
        }

        private void livroToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void frmMainProgram_Load(object sender, EventArgs e) {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e) {
            cadastroCliente.ShowDialog();
        }

        private void deployDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e) {
            consultaCliente.ShowDialog();
        }

        private void deployToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                dao.criaTabelas();
                MessageBox.Show("Tabelas criadas com sucesso", "Aviso");
            }
            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                dao.deletaTabelas();
                MessageBox.Show("O schema public foi removido e recriado com sucesso.", "Aviso");
            }
            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void janelaDoMilagreToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e) {
            cadastroLivro.ShowDialog();
        }

        private void alugarToolStripMenuItem_Click(object sender, EventArgs e) {
            alugaLivro.ShowDialog();
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e) {
            pedidoLivro.ShowDialog();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e) {
            // DEIXAR EM BRANCO
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consultaLivro.ShowDialog();
        }

        private void sobreOBibliotechToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("O Bibliotech é um sistema integrado para controle bibliotecário, que visa facilitar a visa dos gerenciadores de biblioteca. Você pode cadastrar usuários e livros, de acordo com o tipo do livro, editora e várias coisas mais. ", "Sobre o Bibliotech");
        }
    }
}
