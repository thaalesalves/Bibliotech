﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBiblioteca {
    class Metodos {
        public string fixEncoding(string text) {
            string textFixed;
            byte[] bytes = Encoding.Default.GetBytes(text);
            textFixed = Encoding.UTF8.GetString(bytes);
            return textFixed;
        }

        public static System.Boolean isNumeric(System.Object Expression) { // Checador de tipo de campo
            if (Expression is string) {
                return false;
            } else if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double) {
                return true;
            }
            return false;
        }
    }
}
