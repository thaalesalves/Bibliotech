﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;

namespace sistemaBiblioteca {
    class consultaLivroDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

        /* Construtor */
        public consultaLivroDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            );
        }

        /* Declaração de Variáveis */
        static string connString = null;

        /* Métodos */
        public DataTable getRegistro() { // Busca todos os usuários do banco de dados
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT livro.*, estoque.qtd FROM livro, estoque";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public DataTable getRegistroID(string busca) { // Busca usuários do banco por ID
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT *, estoque.qtd FROM livro, estoque WHERE livro.id = " + busca + ";";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public DataTable getRegistroNome(string busca) { // Busca usuários do banco por nome
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT * FROM livro WHERE livro.titulo ILIKE %" + busca + "%;";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public void deletaRegistro(int id) { // Deleta registro de usuário
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdDeletar = String.Format("DELETE FROM livro WHERE id = " + id + ";");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdDeletar, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }

        public string getRegistroTesteNome(int id) {
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT titulo FROM usuario WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return Convert.ToString(dt);
        }

        public DataTable getRegistroTeste(int id) { // Busca o nome do usuário
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT titulo FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public string getAutorNome(int id) { // Retorna o último nome do usuário
            string autor;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT autor FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        autor = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return autor;
        }

        public string getTitulo(int id) { // Retorna o último nome do usuário
            string titulo;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT titulo FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        titulo = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return titulo;
        }

        public string getAutorSobrenome(int id) { // Retorna o último nome do usuário
            string autor;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT ultimonome FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        autor = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return autor;
        }

        public string getGenero(int id) { // Retorna o último nome do usuário
            string genero;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT genero FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        genero = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return genero;
        }

        public string getEditora(int id) { // Retorna o último nome do usuário
            string editora;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT editora FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        editora = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return editora;
        }

        public string getAno(int id) { // Retorna o último nome do usuário
            string ano;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT ano FROM livro WHERE livro.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        ano = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return ano;
        }

        public void atualizaLivro(int id, string titulo, int ano, string genero, string editora, string autor, string ultimoNome) { // Atualiza livros no banco
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format("UPDATE livro SET titulo = '" + titulo + "', genero = '" + genero + "', editora = '" + editora + "', ano = " + ano + ", autor = '" + autor + "', ultimoNome = '" + ultimoNome + "' WHERE id = " + id + ";");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }
    }
}