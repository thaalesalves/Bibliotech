﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class consultaLivro : Form {
        /* Instanciação de Objetos */
        consultaLivroDAO dao = new consultaLivroDAO();
        perfilLivro perfilLivro = new perfilLivro();
        atualizaLivro atualizaLivro = new atualizaLivro();

        /* Variáveis */
        private int idConvert;

        /* Construtor */
        public consultaLivro() {
            InitializeComponent();
        }

        /* Métodos */
        private void atualizarExibicaoID() { // Método de atualizar a data grid view com os elementos específicos
            dgvTesteExibicao.DataSource = dao.getRegistroID(txtBusca.Text);
        }

        private void atualizarExibicao() { // Método de atualizar a data grid view com todos os elementos
            dgvTesteExibicao.DataSource = dao.getRegistro();
        }

        /* Forms */
        private void btnEditar_Click(object sender, EventArgs e) {
            try
            {
                if (idConvert == 0)
                {
                    MessageBox.Show("Erro: nenhum ID de livro foi selecionado.", "Erro");
                }
                else
                {
                    atualizaLivro.setCampos(idConvert, dao.getTitulo(idConvert), dao.getGenero(idConvert), dao.getAno(idConvert), dao.getEditora(idConvert), dao.getAutorNome(idConvert), dao.getAutorSobrenome(idConvert));
                    atualizaLivro.ShowDialog();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex, "Erro");
            }

            finally {
                atualizarExibicao();
            }
        }

        private void btnRemover_Click(object sender, EventArgs e) {
            try {
                if (idConvert == 0) {
                    MessageBox.Show("Erro: nenhum ID de livro foi selecionado.", "Erro");
                } else {
                    dao.deletaRegistro(idConvert);
                    MessageBox.Show("Registro " + idConvert + " removido com sucesso.", "Aviso");
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }

            finally {
                atualizarExibicao();
            }
        }

        private void btnExibir_Click(object sender, EventArgs e) {
            try {
                atualizarExibicao();
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void btnBusca_Click(object sender, EventArgs e) {
            try {
                if (txtBusca.Text == string.Empty || txtBusca.Text == "Buscar") {
                    MessageBox.Show("Erro: o campo de busca não pode estar em branco.", "Erro");
                } else {
                    dao.getRegistroID(txtBusca.Text);
                    atualizarExibicaoID();
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void dgvTesteExibicao_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            try {
                idConvert = Convert.ToInt32(dgvTesteExibicao.Rows[e.RowIndex].Cells[0].Value.ToString());
                atualizaLivro.Id = idConvert;
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }

        private void consultaLivro_Load(object sender, EventArgs e) {
            MaximizeBox = false;
            MinimizeBox = false;
        }

        private void txtBusca_Click(object sender, EventArgs e) {
            txtBusca.Text = string.Empty;
        }

        private void btnDados_Click(object sender, EventArgs e) {
            try {
                if (idConvert == 0) {
                    MessageBox.Show("Erro: nenhum ID de livro foi selecionado.", "Erro");
                } else {
                    perfilLivro.setCampos(dao.getTitulo(idConvert), dao.getGenero(idConvert), dao.getEditora(idConvert), dao.getAutorNome(idConvert), dao.getAutorSobrenome(idConvert), dao.getAno(idConvert));
                    perfilLivro.ShowDialog();
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }
    }
}
