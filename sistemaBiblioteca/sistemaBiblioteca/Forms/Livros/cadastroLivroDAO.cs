﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;

namespace sistemaBiblioteca {
    class cadastroLivroDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

        /* Declaração de Variáveis */
        static string connString = null;
        int id;

        /* Construtor */
        public cadastroLivroDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            );
        }    

        /* Métodos de Usuário */
        public int getLivroID() {
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    NpgsqlCommand Command = new NpgsqlCommand("SELECT id FROM livro ORDER BY livro.id DESC LIMIT 1;", pgsqlConnection);
                    NpgsqlDataReader result = Command.ExecuteReader();

                    while (result.Read()) {
                        id = result.GetOrdinal("id");
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

                finally {
                    pgsqlConnection.Close();
                }
            return id;
        }

        public void cadastraLivro(int qtd, string titulo, int ano, string genero, string editora, string autor, string ultimoNome) { // Cadastra livros no banco
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format("INSERT INTO livro VALUES(DEFAULT, '" + titulo + "','" + genero + "','" + editora + "'," + ano + ",'" + autor + "','" + ultimoNome + "'); INSERT INTO estoque VALUES(DEFAULT, " + qtd + ");");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
               pgsqlConnection.Close();
            }
        }        
    }
}
