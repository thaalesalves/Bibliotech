﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace sistemaBiblioteca { 
    class alugaLivroDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

        /* Construtor */
        public alugaLivroDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            );  
        }

        /* Declaração de Variáveis */
        static string connString = null;

        /* Métodos */
        public void alugaLivro(int qtdNovo, int id_livro, int id_user, string data_alu, string data_devol) {           
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format("UPDATE estoque SET qtd = " + qtdNovo + " WHERE id_livro = " + id_livro + ";  INSERT INTO auxAluga VALUES(DEFAULT, " + id_livro + ", " + id_user + ", '" + data_alu + "', '" + data_devol + "');");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
               pgsqlConnection.Close();
            }
        }

        public string getUserName(int id) {
            string nome;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT nome FROM usuario WHERE usuario.id = " + id + "; ";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        nome = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return nome;
        }

        public int getLivroID(string titulo) {
            int id = 0;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT id FROM livro WHERE livro.titulo ILIKE '" + titulo + "'; ";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        id = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return id;
        }

        public int getLivroQtd(int id) {
            int qtd = 0;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT qtd FROM estoque WHERE estoque.id_livro = " + id + "; ";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        qtd = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return qtd;
        }
    }
}
