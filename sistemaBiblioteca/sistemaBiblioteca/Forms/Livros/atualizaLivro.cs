﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class atualizaLivro : Form {
        /* Construtor */
        public atualizaLivro() {
            InitializeComponent();

            lblSeparadorDados.AutoSize = false; // Separador de Dados Literários
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;

            lblSeparadorAutor.AutoSize = false; // Separador de Dados do Autor
            lblSeparadorAutor.Height = 2;
            lblSeparadorAutor.BorderStyle = BorderStyle.Fixed3D;
        }

        /* Instanciação de Objetos */
        Metodos mtd = new Metodos();
        consultaLivroDAO dao = new consultaLivroDAO();

        /* Variáveis */
        private string titulo, genero, editora, nomeAutor, sobrenomeAutor;
        private int ano, id;

        /* Métodos */
        public int Id {
            get { return id; }
            set { id = value; } 
        }

        public void setCampos(int id, string titulo, string genero, string ano, string editora, string autor, string ultimoNome) {
            this.Text = "Edição de " + titulo;
            txtTitulo.Text = titulo;
            comboGenero.Text = genero;
            txtLancamento.Text = ano;
            txtEditora.Text = editora;
            txtNomeAutor.Text = autor;
            txtSobrenomeAutor.Text = ultimoNome;
            this.id = id;
        }

        /* Forms */
        private void btnCadastrar_Click(object sender, EventArgs e) {
            ano = Convert.ToInt32(txtLancamento.Text);
            titulo = mtd.fixEncoding(txtTitulo.Text);
            genero = mtd.fixEncoding(comboGenero.Text);
            editora = mtd.fixEncoding(txtEditora.Text);
            nomeAutor = mtd.fixEncoding(txtNomeAutor.Text);
            sobrenomeAutor = mtd.fixEncoding(txtSobrenomeAutor.Text);
            dao.atualizaLivro(id, titulo, ano, genero, editora, nomeAutor, sobrenomeAutor);
            MessageBox.Show("Os dados de " + titulo + " foram atualizados com sucesso", "Aviso");
            this.Close();
        }

        private void txtTitulo_TextChanged(object sender, EventArgs e) {

        }

        private void comboGenero_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void txtEditora_TextChanged(object sender, EventArgs e) {

        }

        private void txtLancamento_TextChanged(object sender, EventArgs e) {

        }

        private void txtSobrenomeAutor_TextChanged(object sender, EventArgs e) {

        }

        private void txtNomeAutor_TextChanged(object sender, EventArgs e) {

        }

        private void atualizaLivro_Load(object sender, EventArgs e) {
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void txtTitulo_Click(object sender, EventArgs e) {
            txtTitulo.Text = string.Empty;
        }

        private void txtLancamento_Click(object sender, EventArgs e) {
            txtLancamento.Text = string.Empty;
        }

        private void txtEditora_Click(object sender, EventArgs e) {
            txtEditora.Text = string.Empty;            
        }

        private void txtNomeAutor_Click(object sender, EventArgs e) {
            txtNomeAutor.Text = string.Empty;
        }

        private void txtSobrenomeAutor_Click(object sender, EventArgs e) {
            txtSobrenomeAutor.Text = string.Empty;
        }
    }
}
