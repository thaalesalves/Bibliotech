﻿namespace sistemaBiblioteca
{
    partial class alugaLivro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(alugaLivro));
            this.btnAlugar = new System.Windows.Forms.Button();
            this.comboLivros = new System.Windows.Forms.ComboBox();
            this.txtIDCliente = new System.Windows.Forms.TextBox();
            this.lblIDCliente = new System.Windows.Forms.Label();
            this.lblLivro = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAlugar
            // 
            this.btnAlugar.Location = new System.Drawing.Point(157, 131);
            this.btnAlugar.Name = "btnAlugar";
            this.btnAlugar.Size = new System.Drawing.Size(75, 23);
            this.btnAlugar.TabIndex = 0;
            this.btnAlugar.Text = "Alugar";
            this.btnAlugar.UseVisualStyleBackColor = true;
            this.btnAlugar.Click += new System.EventHandler(this.btnAlugar_Click);
            // 
            // comboLivros
            // 
            this.comboLivros.FormattingEnabled = true;
            this.comboLivros.Location = new System.Drawing.Point(48, 41);
            this.comboLivros.Name = "comboLivros";
            this.comboLivros.Size = new System.Drawing.Size(184, 21);
            this.comboLivros.TabIndex = 1;
            // 
            // txtIDCliente
            // 
            this.txtIDCliente.Location = new System.Drawing.Point(86, 12);
            this.txtIDCliente.Name = "txtIDCliente";
            this.txtIDCliente.Size = new System.Drawing.Size(146, 20);
            this.txtIDCliente.TabIndex = 2;
            // 
            // lblIDCliente
            // 
            this.lblIDCliente.AutoSize = true;
            this.lblIDCliente.Location = new System.Drawing.Point(12, 15);
            this.lblIDCliente.Name = "lblIDCliente";
            this.lblIDCliente.Size = new System.Drawing.Size(68, 13);
            this.lblIDCliente.TabIndex = 3;
            this.lblIDCliente.Text = "ID do Cliente";
            // 
            // lblLivro
            // 
            this.lblLivro.AutoSize = true;
            this.lblLivro.Location = new System.Drawing.Point(12, 44);
            this.lblLivro.Name = "lblLivro";
            this.lblLivro.Size = new System.Drawing.Size(30, 13);
            this.lblLivro.TabIndex = 4;
            this.lblLivro.Text = "Livro";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(12, 94);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(108, 13);
            this.lblData.TabIndex = 5;
            this.lblData.Text = "DATA DO ALUGUEL";
            // 
            // alugaLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 164);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblLivro);
            this.Controls.Add(this.lblIDCliente);
            this.Controls.Add(this.txtIDCliente);
            this.Controls.Add(this.comboLivros);
            this.Controls.Add(this.btnAlugar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "alugaLivro";
            this.Text = "Aluguel de Livros";
            this.Load += new System.EventHandler(this.alugaLivro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAlugar;
        private System.Windows.Forms.ComboBox comboLivros;
        private System.Windows.Forms.TextBox txtIDCliente;
        private System.Windows.Forms.Label lblIDCliente;
        private System.Windows.Forms.Label lblLivro;
        private System.Windows.Forms.Label lblData;
    }
}