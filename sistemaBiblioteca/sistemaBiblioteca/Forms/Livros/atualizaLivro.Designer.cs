﻿namespace sistemaBiblioteca {
    partial class atualizaLivro {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.lblSobrenomeAutor = new System.Windows.Forms.Label();
            this.txtSobrenomeAutor = new System.Windows.Forms.TextBox();
            this.lblNomeAutor = new System.Windows.Forms.Label();
            this.txtNomeAutor = new System.Windows.Forms.TextBox();
            this.lblDadosAutor = new System.Windows.Forms.Label();
            this.lblSeparadorAutor = new System.Windows.Forms.Label();
            this.txtEditora = new System.Windows.Forms.TextBox();
            this.lblEditora = new System.Windows.Forms.Label();
            this.txtLancamento = new System.Windows.Forms.TextBox();
            this.lblLancamento = new System.Windows.Forms.Label();
            this.lblGenero = new System.Windows.Forms.Label();
            this.comboGenero = new System.Windows.Forms.ComboBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.lblDadosLiterarios = new System.Windows.Forms.Label();
            this.lblSeparadorDados = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Location = new System.Drawing.Point(344, 203);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(75, 23);
            this.btnCadastrar.TabIndex = 56;
            this.btnCadastrar.Text = "Atualizar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // lblSobrenomeAutor
            // 
            this.lblSobrenomeAutor.AutoSize = true;
            this.lblSobrenomeAutor.Location = new System.Drawing.Point(175, 171);
            this.lblSobrenomeAutor.Name = "lblSobrenomeAutor";
            this.lblSobrenomeAutor.Size = new System.Drawing.Size(67, 13);
            this.lblSobrenomeAutor.TabIndex = 55;
            this.lblSobrenomeAutor.Text = "Último Nome";
            // 
            // txtSobrenomeAutor
            // 
            this.txtSobrenomeAutor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSobrenomeAutor.Location = new System.Drawing.Point(248, 168);
            this.txtSobrenomeAutor.Name = "txtSobrenomeAutor";
            this.txtSobrenomeAutor.Size = new System.Drawing.Size(100, 20);
            this.txtSobrenomeAutor.TabIndex = 54;
            this.txtSobrenomeAutor.Click += new System.EventHandler(this.txtSobrenomeAutor_Click);
            this.txtSobrenomeAutor.TextChanged += new System.EventHandler(this.txtSobrenomeAutor_TextChanged);
            // 
            // lblNomeAutor
            // 
            this.lblNomeAutor.AutoSize = true;
            this.lblNomeAutor.Location = new System.Drawing.Point(12, 171);
            this.lblNomeAutor.Name = "lblNomeAutor";
            this.lblNomeAutor.Size = new System.Drawing.Size(35, 13);
            this.lblNomeAutor.TabIndex = 53;
            this.lblNomeAutor.Text = "Nome";
            // 
            // txtNomeAutor
            // 
            this.txtNomeAutor.Location = new System.Drawing.Point(53, 168);
            this.txtNomeAutor.Name = "txtNomeAutor";
            this.txtNomeAutor.Size = new System.Drawing.Size(100, 20);
            this.txtNomeAutor.TabIndex = 52;
            this.txtNomeAutor.Click += new System.EventHandler(this.txtNomeAutor_Click);
            this.txtNomeAutor.TextChanged += new System.EventHandler(this.txtNomeAutor_TextChanged);
            // 
            // lblDadosAutor
            // 
            this.lblDadosAutor.AutoSize = true;
            this.lblDadosAutor.Location = new System.Drawing.Point(12, 129);
            this.lblDadosAutor.Name = "lblDadosAutor";
            this.lblDadosAutor.Size = new System.Drawing.Size(81, 13);
            this.lblDadosAutor.TabIndex = 51;
            this.lblDadosAutor.Text = "Dados do Autor";
            // 
            // lblSeparadorAutor
            // 
            this.lblSeparadorAutor.AutoSize = true;
            this.lblSeparadorAutor.Location = new System.Drawing.Point(94, 137);
            this.lblSeparadorAutor.Name = "lblSeparadorAutor";
            this.lblSeparadorAutor.Size = new System.Drawing.Size(325, 13);
            this.lblSeparadorAutor.TabIndex = 50;
            this.lblSeparadorAutor.Text = "ddddddddddddddddddddddddddddddddddddddddddddddddddddd";
            // 
            // txtEditora
            // 
            this.txtEditora.Location = new System.Drawing.Point(232, 80);
            this.txtEditora.Name = "txtEditora";
            this.txtEditora.Size = new System.Drawing.Size(101, 20);
            this.txtEditora.TabIndex = 49;
            this.txtEditora.Click += new System.EventHandler(this.txtEditora_Click);
            this.txtEditora.TextChanged += new System.EventHandler(this.txtEditora_TextChanged);
            // 
            // lblEditora
            // 
            this.lblEditora.AutoSize = true;
            this.lblEditora.Location = new System.Drawing.Point(186, 83);
            this.lblEditora.Name = "lblEditora";
            this.lblEditora.Size = new System.Drawing.Size(40, 13);
            this.lblEditora.TabIndex = 48;
            this.lblEditora.Text = "Editora";
            // 
            // txtLancamento
            // 
            this.txtLancamento.Location = new System.Drawing.Point(121, 80);
            this.txtLancamento.Name = "txtLancamento";
            this.txtLancamento.Size = new System.Drawing.Size(45, 20);
            this.txtLancamento.TabIndex = 47;
            this.txtLancamento.Click += new System.EventHandler(this.txtLancamento_Click);
            this.txtLancamento.TextChanged += new System.EventHandler(this.txtLancamento_TextChanged);
            // 
            // lblLancamento
            // 
            this.lblLancamento.AutoSize = true;
            this.lblLancamento.Location = new System.Drawing.Point(12, 83);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.Size = new System.Drawing.Size(103, 13);
            this.lblLancamento.TabIndex = 46;
            this.lblLancamento.Text = "Ano de Lançamento";
            // 
            // lblGenero
            // 
            this.lblGenero.AutoSize = true;
            this.lblGenero.Location = new System.Drawing.Point(168, 51);
            this.lblGenero.Name = "lblGenero";
            this.lblGenero.Size = new System.Drawing.Size(42, 13);
            this.lblGenero.TabIndex = 45;
            this.lblGenero.Text = "Gênero";
            // 
            // comboGenero
            // 
            this.comboGenero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGenero.FormattingEnabled = true;
            this.comboGenero.Items.AddRange(new object[] {
            "Administração",
            "Científico",
            "Ciência da Computação",
            "Comédia",
            "Engenharia",
            "Estudos",
            "Ficção",
            "Ficção Científica",
            "Medicina",
            "Negócios",
            "Ode",
            "Poesia",
            "Romance",
            "Terror"});
            this.comboGenero.Location = new System.Drawing.Point(216, 48);
            this.comboGenero.Name = "comboGenero";
            this.comboGenero.Size = new System.Drawing.Size(82, 21);
            this.comboGenero.TabIndex = 44;
            this.comboGenero.SelectedIndexChanged += new System.EventHandler(this.comboGenero_SelectedIndexChanged);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(12, 51);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(35, 13);
            this.lblTitulo.TabIndex = 43;
            this.lblTitulo.Text = "Título";
            // 
            // txtTitulo
            // 
            this.txtTitulo.Location = new System.Drawing.Point(53, 48);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(100, 20);
            this.txtTitulo.TabIndex = 42;
            this.txtTitulo.Click += new System.EventHandler(this.txtTitulo_Click);
            this.txtTitulo.TextChanged += new System.EventHandler(this.txtTitulo_TextChanged);
            // 
            // lblDadosLiterarios
            // 
            this.lblDadosLiterarios.AutoSize = true;
            this.lblDadosLiterarios.Location = new System.Drawing.Point(12, 9);
            this.lblDadosLiterarios.Name = "lblDadosLiterarios";
            this.lblDadosLiterarios.Size = new System.Drawing.Size(83, 13);
            this.lblDadosLiterarios.TabIndex = 41;
            this.lblDadosLiterarios.Text = "Dados Literários";
            // 
            // lblSeparadorDados
            // 
            this.lblSeparadorDados.AutoSize = true;
            this.lblSeparadorDados.Location = new System.Drawing.Point(94, 17);
            this.lblSeparadorDados.Name = "lblSeparadorDados";
            this.lblSeparadorDados.Size = new System.Drawing.Size(325, 13);
            this.lblSeparadorDados.TabIndex = 40;
            this.lblSeparadorDados.Text = "ddddddddddddddddddddddddddddddddddddddddddddddddddddd";
            // 
            // atualizaLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 234);
            this.Controls.Add(this.btnCadastrar);
            this.Controls.Add(this.lblSobrenomeAutor);
            this.Controls.Add(this.txtSobrenomeAutor);
            this.Controls.Add(this.lblNomeAutor);
            this.Controls.Add(this.txtNomeAutor);
            this.Controls.Add(this.lblDadosAutor);
            this.Controls.Add(this.lblSeparadorAutor);
            this.Controls.Add(this.txtEditora);
            this.Controls.Add(this.lblEditora);
            this.Controls.Add(this.txtLancamento);
            this.Controls.Add(this.lblLancamento);
            this.Controls.Add(this.lblGenero);
            this.Controls.Add(this.comboGenero);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtTitulo);
            this.Controls.Add(this.lblDadosLiterarios);
            this.Controls.Add(this.lblSeparadorDados);
            this.Name = "atualizaLivro";
            this.Text = "atualizaLivro";
            this.Load += new System.EventHandler(this.atualizaLivro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Label lblSobrenomeAutor;
        private System.Windows.Forms.TextBox txtSobrenomeAutor;
        private System.Windows.Forms.Label lblNomeAutor;
        private System.Windows.Forms.TextBox txtNomeAutor;
        private System.Windows.Forms.Label lblDadosAutor;
        private System.Windows.Forms.Label lblSeparadorAutor;
        private System.Windows.Forms.TextBox txtEditora;
        private System.Windows.Forms.Label lblEditora;
        private System.Windows.Forms.TextBox txtLancamento;
        private System.Windows.Forms.Label lblLancamento;
        private System.Windows.Forms.Label lblGenero;
        private System.Windows.Forms.ComboBox comboGenero;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Label lblDadosLiterarios;
        private System.Windows.Forms.Label lblSeparadorDados;
    }
}