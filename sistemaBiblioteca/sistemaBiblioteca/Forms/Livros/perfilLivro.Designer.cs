﻿namespace sistemaBiblioteca {
    partial class perfilLivro {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblDados = new System.Windows.Forms.Label();
            this.lblSeparadorDados = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.lblGenero = new System.Windows.Forms.Label();
            this.lblAno = new System.Windows.Forms.Label();
            this.lblEditora = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.Location = new System.Drawing.Point(12, 9);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(83, 13);
            this.lblDados.TabIndex = 35;
            this.lblDados.Text = "Dados Literários";
            // 
            // lblSeparadorDados
            // 
            this.lblSeparadorDados.AutoSize = true;
            this.lblSeparadorDados.Location = new System.Drawing.Point(94, 17);
            this.lblSeparadorDados.Name = "lblSeparadorDados";
            this.lblSeparadorDados.Size = new System.Drawing.Size(265, 13);
            this.lblSeparadorDados.TabIndex = 34;
            this.lblSeparadorDados.Text = "ddddddddddddddddddddddddddddddddddddddddddd";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.Location = new System.Drawing.Point(26, 82);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(42, 13);
            this.lblAutor.TabIndex = 30;
            this.lblAutor.Text = "lblAutor";
            // 
            // lblGenero
            // 
            this.lblGenero.AutoSize = true;
            this.lblGenero.Location = new System.Drawing.Point(26, 64);
            this.lblGenero.Name = "lblGenero";
            this.lblGenero.Size = new System.Drawing.Size(52, 13);
            this.lblGenero.TabIndex = 29;
            this.lblGenero.Text = "lblGenero";
            // 
            // lblAno
            // 
            this.lblAno.AutoSize = true;
            this.lblAno.Location = new System.Drawing.Point(26, 46);
            this.lblAno.Name = "lblAno";
            this.lblAno.Size = new System.Drawing.Size(36, 13);
            this.lblAno.TabIndex = 28;
            this.lblAno.Text = "lblAno";
            // 
            // lblEditora
            // 
            this.lblEditora.AutoSize = true;
            this.lblEditora.Location = new System.Drawing.Point(236, 46);
            this.lblEditora.Name = "lblEditora";
            this.lblEditora.Size = new System.Drawing.Size(50, 13);
            this.lblEditora.TabIndex = 27;
            this.lblEditora.Text = "lblEditora";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(26, 28);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(43, 13);
            this.lblNome.TabIndex = 26;
            this.lblNome.Text = "lblTitulo";
            // 
            // perfilLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 104);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.lblSeparadorDados);
            this.Controls.Add(this.lblAutor);
            this.Controls.Add(this.lblGenero);
            this.Controls.Add(this.lblAno);
            this.Controls.Add(this.lblEditora);
            this.Controls.Add(this.lblNome);
            this.Name = "perfilLivro";
            this.Text = "perfilLivro";
            this.Load += new System.EventHandler(this.perfilLivro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Label lblSeparadorDados;
        private System.Windows.Forms.Label lblAutor;
        private System.Windows.Forms.Label lblGenero;
        private System.Windows.Forms.Label lblAno;
        private System.Windows.Forms.Label lblEditora;
        private System.Windows.Forms.Label lblNome;
    }
}