﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class alugaLivro : Form {
        /* Construtor */
        public alugaLivro() {
            InitializeComponent();                 
            dataAluguel = DateTime.Now.ToString("dd/MM/yyyy");
            dataDevolucao = DateTime.Now.AddDays(7).ToString("dd/MM/yyyy");
            lblData.Text = "Data de aluguel: " + dataAluguel + ".\nData de devolução: " + dataDevolucao + ".";
        }

        /* Instanciação de Objetos */
        Metodos mtd = new Metodos();
        alugaLivroDAO dao = new alugaLivroDAO();

        /* Variáveis */
        private string dataAluguel, dataDevolucao, userName;
        private int userID, livroID, livroQtd;
        
        /* Forms */
        private void alugaLivro_Load(object sender, EventArgs e) {
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void btnAlugar_Click(object sender, EventArgs e) {
            userName = dao.getUserName(userID);
            try {
                if (dao.getLivroQtd(livroID) <= 0) {
                    MessageBox.Show("O livro " + comboLivros.Text + " está em falta no estoque.", "Aviso");
                } else {
                    userID = Convert.ToInt32(txtIDCliente.Text);                
                livroID = dao.getLivroID(comboLivros.Text);
                livroQtd = dao.getLivroQtd(livroID) - 1;
                dao.alugaLivro(livroQtd, livroID, userID, dataAluguel, dataDevolucao);
                MessageBox.Show("O livro " + comboLivros.Text + " foi alugado para " + userName + ", e deve ser devolvido em " + dataDevolucao + ".", "Aluguel de Livro");
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }
    }
}
