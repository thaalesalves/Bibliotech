﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace sistemaBiblioteca {
    public partial class cadastroLivro : Form {
        /* Construtor */
        public cadastroLivro() {
            InitializeComponent();

            lblSeparadorDados.AutoSize = false; // Separador de Dados Literários
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;

            lblSeparadorAutor.AutoSize = false; // Separador de Dados do Autor
            lblSeparadorAutor.Height = 2;
            lblSeparadorAutor.BorderStyle = BorderStyle.Fixed3D;
        }

        /* Instanciação */
        cadastroLivroDAO dao = new cadastroLivroDAO();
        Metodos mtd = new Metodos();

        /* Variáveis */
        private string editora, nomeAutor, sobrenomeAutor, titulo, genero;
        private int ano, qtd;

        private void cadastroLivro_Load(object sender, EventArgs e) {

        }

        private void label1_Click(object sender, EventArgs e) {

        }

        private void btnCadastrar_Click(object sender, EventArgs e) {
            try {
                ano = Convert.ToInt32(txtLancamento.Text); 
                titulo = mtd.fixEncoding(txtTitulo.Text);
                genero = mtd.fixEncoding(comboGenero.Text);
                editora = mtd.fixEncoding(txtEditora.Text);
                nomeAutor = mtd.fixEncoding(txtNomeAutor.Text);
                sobrenomeAutor = mtd.fixEncoding(txtSobrenomeAutor.Text);
                qtd = Convert.ToInt32(txtQtd.Text);              
                dao.cadastraLivro(qtd, titulo, ano, genero, editora, nomeAutor, sobrenomeAutor);
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }

            finally {
                MessageBox.Show("Livro " + titulo + " cadastrado com sucesso.", "Aviso");
            }
        }

        private void lblSeparadorDados_Click(object sender, EventArgs e) {

        }

        private void lblDadosLiterarios_Click(object sender, EventArgs e) {

        }

        private void txtTitulo_TextChanged(object sender, EventArgs e) {

        }

        private void lblTitulo_Click(object sender, EventArgs e) {

        }

        private void comboGenero_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void lblGenero_Click(object sender, EventArgs e) {

        }

        private void lblLancamento_Click(object sender, EventArgs e) {

        }

        private void txtLancamento_TextChanged(object sender, EventArgs e) {

        }

        private void lblEditora_Click(object sender, EventArgs e) {

        }

        private void txtEditora_TextChanged(object sender, EventArgs e) {

        }

        private void lblDadosAutor_Click(object sender, EventArgs e) {

        }

        private void lblSeparadorAutor_Click(object sender, EventArgs e) {

        }

        private void txtNomeAutor_TextChanged(object sender, EventArgs e) {

        }

        private void lblSobrenomeAutor_Click(object sender, EventArgs e) {

        }

        private void txtSobrenomeAutor_TextChanged(object sender, EventArgs e) {

        }
    }
}
