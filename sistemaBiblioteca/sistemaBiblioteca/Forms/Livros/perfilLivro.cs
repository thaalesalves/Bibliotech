﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class perfilLivro : Form {
        /* Instanciação de Objetos */
        consultaClienteDAO dao = new consultaClienteDAO();

        /* Construtor */
        public perfilLivro() {
            InitializeComponent();

            /* Separadores */
            lblSeparadorDados.AutoSize = false; // Separador de Dados Pessoais
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;
        }

        /* Métodos */
        public void setCampos(string titulo, string genero, string editora, string autorNome, string autorSobrenome, string ano) {
            this.Text = "Propriedades de " + titulo;
            lblNome.Text = "Nome: " + titulo;
            lblGenero.Text = "Gênero: " + genero;
            lblEditora.Text = "Editora: " + editora;
            lblAno.Text = String.Format("Ano de Publicação: " + Convert.ToString(ano));
            lblAutor.Text = String.Format("Nome do Autor: " + autorSobrenome.ToUpper() + ", " + autorNome);
        }

        /* Forms */
        private void perfilLivro_Load(object sender, EventArgs e) {

        }
    }
}
