﻿namespace sistemaBiblioteca
{
    partial class consultaLivro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(consultaLivro));
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnRemover = new System.Windows.Forms.Button();
            this.txtBusca = new System.Windows.Forms.TextBox();
            this.btnBusca = new System.Windows.Forms.Button();
            this.btnExibir = new System.Windows.Forms.Button();
            this.dgvTesteExibicao = new System.Windows.Forms.DataGridView();
            this.btnDados = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTesteExibicao)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(255, 38);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 11;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnRemover
            // 
            this.btnRemover.Location = new System.Drawing.Point(336, 38);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(75, 23);
            this.btnRemover.TabIndex = 10;
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // txtBusca
            // 
            this.txtBusca.Location = new System.Drawing.Point(12, 12);
            this.txtBusca.Name = "txtBusca";
            this.txtBusca.Size = new System.Drawing.Size(399, 20);
            this.txtBusca.TabIndex = 9;
            this.txtBusca.Text = "Buscar";
            this.txtBusca.Click += new System.EventHandler(this.txtBusca_Click);
            // 
            // btnBusca
            // 
            this.btnBusca.Location = new System.Drawing.Point(12, 38);
            this.btnBusca.Name = "btnBusca";
            this.btnBusca.Size = new System.Drawing.Size(75, 23);
            this.btnBusca.TabIndex = 8;
            this.btnBusca.Text = "Buscar ID";
            this.btnBusca.UseVisualStyleBackColor = true;
            this.btnBusca.Click += new System.EventHandler(this.btnBusca_Click);
            // 
            // btnExibir
            // 
            this.btnExibir.Location = new System.Drawing.Point(93, 38);
            this.btnExibir.Name = "btnExibir";
            this.btnExibir.Size = new System.Drawing.Size(75, 23);
            this.btnExibir.TabIndex = 7;
            this.btnExibir.Text = "Exibir Tudo";
            this.btnExibir.UseVisualStyleBackColor = true;
            this.btnExibir.Click += new System.EventHandler(this.btnExibir_Click);
            // 
            // dgvTesteExibicao
            // 
            this.dgvTesteExibicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTesteExibicao.Location = new System.Drawing.Point(12, 67);
            this.dgvTesteExibicao.Name = "dgvTesteExibicao";
            this.dgvTesteExibicao.ReadOnly = true;
            this.dgvTesteExibicao.Size = new System.Drawing.Size(399, 150);
            this.dgvTesteExibicao.TabIndex = 6;
            this.dgvTesteExibicao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTesteExibicao_CellContentClick);
            // 
            // btnDados
            // 
            this.btnDados.Location = new System.Drawing.Point(174, 38);
            this.btnDados.Name = "btnDados";
            this.btnDados.Size = new System.Drawing.Size(75, 23);
            this.btnDados.TabIndex = 12;
            this.btnDados.Text = "Dados";
            this.btnDados.UseVisualStyleBackColor = true;
            this.btnDados.Click += new System.EventHandler(this.btnDados_Click);
            // 
            // consultaLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 228);
            this.Controls.Add(this.btnDados);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnRemover);
            this.Controls.Add(this.txtBusca);
            this.Controls.Add(this.btnBusca);
            this.Controls.Add(this.btnExibir);
            this.Controls.Add(this.dgvTesteExibicao);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "consultaLivro";
            this.Text = "consultaLivro";
            this.Load += new System.EventHandler(this.consultaLivro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTesteExibicao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.TextBox txtBusca;
        private System.Windows.Forms.Button btnBusca;
        private System.Windows.Forms.Button btnExibir;
        private System.Windows.Forms.DataGridView dgvTesteExibicao;
        private System.Windows.Forms.Button btnDados;
    }
}