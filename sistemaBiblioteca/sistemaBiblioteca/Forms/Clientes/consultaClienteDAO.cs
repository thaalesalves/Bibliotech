﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;

namespace sistemaBiblioteca {
    class consultaClienteDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

        /* Declaração de Variáveis */
        static string connString = null;

        /* Construtor */
        public consultaClienteDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            );
        }

        /* Métodos */
        public DataTable getRegistro() { // Busca todos os usuários do banco de dados
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT * FROM usuario";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }            
            return dt;
        }

        public DataTable getRegistroID(string busca) { // Busca usuários do banco por ID
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT * FROM usuario WHERE usuario.id = " + busca + ";";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public DataTable getRegistroNome(string busca) { // Busca usuários do banco por nome
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT * FROM usuario WHERE usuario.nome ILIKE %" + busca + "%;";
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        public void deletaRegistro(int id) { // Deleta registro de usuário
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdDeletar = String.Format("DELETE FROM auxAluga WHERE id = " + id + "; DELETE FROM usuario WHERE id = " + id + ";");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdDeletar, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }

        public string getRegistroNomeCompleto(int id) { // Retorna o nome completo do usuário
            string firstName, secondName, name;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT nome, sobrenome FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        firstName = dt.Rows[0].ItemArray[0].ToString();
                        secondName = dt.Rows[0].ItemArray[1].ToString();
                        name = String.Format(firstName + " " + secondName);
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return name;
        }        

        public void atualizaCadastro(int id, string endereco, int numero, string uf, string cep, string cidade, string fixo, string celular, string email, string website) { // Atualiza cadastro de usuários do banco
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string pgsqlCommand = "UPDATE usuario SET endereco = '" + endereco + "', numero = " + numero + ", uf = '" + uf + "', cep = '" + cep + "', cidade = '" + cidade + "', fixo = '" + fixo + "', celular = '" + celular + "', email = '" + email + "', website = '" + website + "' WHERE id = " + id + "; ";
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(pgsqlCommand, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
        }

        public string getNome(int id) { // Retorna o primeiro nome do usuário
            string name;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT nome FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        name = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }
                
            catch (Exception ex) {
                throw ex;
            }
            return name;
        }

        public string getSobrenome(int id) { // Retorna o último nome do usuário
            string surname;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT sobrenome FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        surname = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return surname;
        }

        public string getRG(int id) { // Retorna o RG do usuário
            string rg;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT rg FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        rg = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return rg;
        }

        public string getCPF(int id) { // Retorna o CPF do usuário
            string cpf;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT cpf FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        cpf = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }
            return cpf;
        }

        public string getEndereco(int id) {
            string endereco;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT endereco FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        endereco = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return endereco;
        }

        public string getCidade(int id) {
            string cidade;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT cidade FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        cidade = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return cidade;
        }

        public string getNumero(int id) {
            string num;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT numero FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        num = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return num;
        }

        public string getUF(int id) {
            string uf;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT uf FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        uf = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return uf;
        }

        public string getCEP(int id) {
            string cep;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT cep FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        cep = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return cep;
        }

        public string getTelFixo(int id) {
            string fixo;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT fixo FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        fixo = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return fixo;
        }

        public string getTelCel(int id) {
            string cel;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT celular FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        cel = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return cel;
        }

        public string getSite(int id) {
            string site;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT website FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        site = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return site;
        }

        public string getEmail(int id) {
            string email;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT email FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        email = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return email;
        }

        public string getNasc(int id) {
            string nasc;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT nascimento FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        nasc = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return nasc;
        }

        public string getSexo(int id) {
            string sex;
            DataTable dt = new DataTable();
            try {
                using (pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdSeleciona = "SELECT sexo FROM usuario WHERE usuario.id = " + id;
                    using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection)) {
                        Adpt.Fill(dt);
                        sex = dt.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
                pgsqlConnection.Close();
            }
            return sex;
        }

        public void atualizaEstoque(int id)
        { // Atualiza cadastro de usuários do banco
            try
            {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString))
                {
                    pgsqlConnection.Open();
                    string pgsqlCommand = "UPDATE estoque SET qtd = qtd + 1; ";
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(pgsqlCommand, pgsqlConnection))
                    {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
