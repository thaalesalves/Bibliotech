﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace sistemaBiblioteca {
    public partial class atualizaCliente : Form {
        /* Construtor */
        public atualizaCliente() {
            InitializeComponent();

            /* Separadores */
            lblSeparadorDados.AutoSize = false; // Separador de Dados Pessoais
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;

            lblSeparadorContato.AutoSize = false; // Separador de Dados de Contato
            lblSeparadorContato.Height = 2;
            lblSeparadorContato.BorderStyle = BorderStyle.Fixed3D;

            /* Máscaras */
            maskedCEP.Mask = "00000-000";
            maskedCEP.Text = "_____-___";
        }

        /* Instanciação de Objetos */
        consultaClienteDAO dao = new consultaClienteDAO(); // objeto de acesso ao banco de dados
        Metodos mtd = new Metodos(); // Objeto da superclasse de métodos
        
        /* Declação de Variáveis */
        private string endereco, uf, cidade, cep; // Variáveis dos dados pessoais
        private string fixo, celular, website, email; // Variáveis dos dados de contato
        private int id, numero; // Declaração das variáveis inteiras

        /* Métodos */
        public int Id {
            get { return id; }
            set { id = value; }
        }

        /* Métodos */
        public void setCampos(string nome, string sobrenome, string rg, string cpf, string endereco, string cep, 
            string cidade, string uf, string fixo, string cel, string site, string email, string num, string sex, string nasc) {
            this.Text = String.Format("Atualização de Cadastro de " + nome);
            txtNome.Text = nome;
            txtSobrenome.Text = sobrenome;
            maskedRG.Text = rg;
            maskedCPF.Text = cpf;
            maskedCEP.Text = cep;
            txtCidade.Text = cidade;
            txtEndereco.Text = endereco;
            comboUF.Text = uf;
            maskedTelCel.Text = cel;
            maskedTelFixo.Text = fixo;
            maskedEmail.Text = email;
            maskedWebsite.Text = site;
            maskedNumero.Text = num;
            txtSexo.Text = sex;
            maskedNascimento.Text = nasc;
        }

        /* Forms */
        private void cadastroCliente_Load(object sender, EventArgs e) {
            MaximizeBox = false;
            MinimizeBox = false;            
        }

        private void txtNome_Click(object sender, EventArgs e) {
            
        }

        private void txtSobrenome_Click(object sender, EventArgs e) {
                       
        }

        private void lblEndereco_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtEndereco_Click(object sender, EventArgs e) {
            txtEndereco.Text = string.Empty;
        }

        private void txtCidade_Click(object sender, EventArgs e) {
            txtCidade.Text = string.Empty;
        }

        private void txtNumero_Click(object sender, EventArgs e) {
            
        }

        private void maskedNumero_Click(object sender, EventArgs e) {
            maskedNumero.Text = string.Empty;
        }

        private void btnCadastrar_Click(object sender, EventArgs e) {
            try {
                if (maskedWebsite.Text == string.Empty || maskedWebsite.Text == null || maskedWebsite.Text == "www.dominio.com") {
                    website = "nenhum";
                } else {
                    website = maskedWebsite.Text;
                }

                if (maskedEmail.Text == string.Empty || maskedEmail.Text == null || maskedEmail.Text == "usuario@dominio.com") {
                    email = "nenhum";
                } else {
                    email = maskedEmail.Text;
                }

                celular = maskedTelCel.Text;
                fixo = maskedTelFixo.Text;
                cidade = mtd.fixEncoding(txtCidade.Text);
                numero = Convert.ToInt32(maskedNumero.Text);
                endereco = mtd.fixEncoding(txtEndereco.Text);
                uf = comboUF.Text;
                cep = maskedCEP.Text;
                dao.atualizaCadastro(id, endereco, numero, uf, cep, cidade, fixo, celular, email, website);
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }

            finally {
                MessageBox.Show("Operação concluída com sucesso.", "Aviso");
                this.Close();
            }
        }

        private void txtCPF_Click(object sender, EventArgs e) {
            //txtCPF.Text = string.Empty;
        }

        private void txtRG_TextChanged(object sender, EventArgs e) {
            //txtCPF.Text = string.Empty;
        }

        private void lblSeparadorDados_Click(object sender, EventArgs e) {
            lblSeparadorDados.AutoSize = false;
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;
        }

        private void label2_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void label3_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtRG_Click(object sender, EventArgs e) {
            //txtRG.Text = string.Empty;
        }

        private void txtTelFixo_TextChanged(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtTelFixo_Click(object sender, EventArgs e) {
            //txtTelFixo.Text = string.Empty;
        }

        private void txtTelCel_Click(object sender, EventArgs e) {
            //txtTelCel.Text = string.Empty;
        }

        private void txtWebsite_Click(object sender, EventArgs e) {
            //txtWebsite.Text = string.Empty;
        }

        private void txtEmail_Click(object sender, EventArgs e) {
            //txtEmail.Text = string.Empty;
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e) {

        }

        private void maskedTextBox1_Click(object sender, EventArgs e) {
            
        }

        private void maskedRG_Click(object sender, EventArgs e) {
            
        }

        private void maskedTelCel_Click(object sender, EventArgs e) {
            maskedTelCel.Text = string.Empty;
        }

        private void maskedTelFixo_Click(object sender, EventArgs e) {
            maskedTelFixo.Text = string.Empty;
        }

        private void maskedEmail_Click(object sender, EventArgs e) {
            maskedEmail.Text = string.Empty;
        }

        private void maskedWebsite_MaskInputRejected(object sender, MaskInputRejectedEventArgs e) {
            maskedWebsite.Text = string.Empty;
        }

        private void maskedCEP_Click(object sender, EventArgs e) {
            maskedCEP.Text = string.Empty;
        }

        private void maskedWebsite_Click(object sender, EventArgs e) {
            maskedWebsite.Text = string.Empty;
        }             
    }
}
