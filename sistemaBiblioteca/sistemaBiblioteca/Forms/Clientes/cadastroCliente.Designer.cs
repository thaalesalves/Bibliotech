﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //

namespace sistemaBiblioteca
{
    partial class cadastroCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNome = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtSobrenome = new System.Windows.Forms.TextBox();
            this.lblSobrenome = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblCidade = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.lblUF = new System.Windows.Forms.Label();
            this.comboUF = new System.Windows.Forms.ComboBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblRG = new System.Windows.Forms.Label();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.lblSeparadorDados = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.lblContato = new System.Windows.Forms.Label();
            this.lblSeparadorContato = new System.Windows.Forms.Label();
            this.lblCelular = new System.Windows.Forms.Label();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.maskedCPF = new System.Windows.Forms.MaskedTextBox();
            this.maskedRG = new System.Windows.Forms.MaskedTextBox();
            this.maskedTelFixo = new System.Windows.Forms.MaskedTextBox();
            this.maskedTelCel = new System.Windows.Forms.MaskedTextBox();
            this.maskedWebsite = new System.Windows.Forms.MaskedTextBox();
            this.maskedEmail = new System.Windows.Forms.MaskedTextBox();
            this.maskedCEP = new System.Windows.Forms.MaskedTextBox();
            this.comboSexo = new System.Windows.Forms.ComboBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.maskedNascimento = new System.Windows.Forms.MaskedTextBox();
            this.maskedNumero = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(20, 36);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(35, 13);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(61, 33);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(100, 20);
            this.txtNome.TabIndex = 1;
            this.txtNome.Text = "Geraldo";
            this.txtNome.Click += new System.EventHandler(this.txtNome_Click);
            // 
            // txtSobrenome
            // 
            this.txtSobrenome.Location = new System.Drawing.Point(239, 33);
            this.txtSobrenome.Name = "txtSobrenome";
            this.txtSobrenome.Size = new System.Drawing.Size(100, 20);
            this.txtSobrenome.TabIndex = 3;
            this.txtSobrenome.Text = "Tadeu da Silva";
            this.txtSobrenome.Click += new System.EventHandler(this.txtSobrenome_Click);
            // 
            // lblSobrenome
            // 
            this.lblSobrenome.AutoSize = true;
            this.lblSobrenome.Location = new System.Drawing.Point(172, 36);
            this.lblSobrenome.Name = "lblSobrenome";
            this.lblSobrenome.Size = new System.Drawing.Size(61, 13);
            this.lblSobrenome.TabIndex = 2;
            this.lblSobrenome.Text = "Sobrenome";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Location = new System.Drawing.Point(20, 62);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(53, 13);
            this.lblEndereco.TabIndex = 4;
            this.lblEndereco.Text = "Endereço";
            this.lblEndereco.Click += new System.EventHandler(this.lblEndereco_Click);
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(79, 59);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(166, 20);
            this.txtEndereco.TabIndex = 5;
            this.txtEndereco.Text = "Avenida Brigadeiro Faria Lima";
            this.txtEndereco.Click += new System.EventHandler(this.txtEndereco_Click);
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(251, 62);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(44, 13);
            this.lblNumero.TabIndex = 6;
            this.lblNumero.Text = "Número";
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Location = new System.Drawing.Point(20, 88);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(40, 13);
            this.lblCidade.TabIndex = 8;
            this.lblCidade.Text = "Cidade";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(66, 85);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(100, 20);
            this.txtCidade.TabIndex = 9;
            this.txtCidade.Text = "São Paulo";
            this.txtCidade.Click += new System.EventHandler(this.txtCidade_Click);
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Location = new System.Drawing.Point(172, 88);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(21, 13);
            this.lblUF.TabIndex = 10;
            this.lblUF.Text = "UF";
            // 
            // comboUF
            // 
            this.comboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboUF.FormattingEnabled = true;
            this.comboUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.comboUF.Location = new System.Drawing.Point(199, 85);
            this.comboUF.Name = "comboUF";
            this.comboUF.Size = new System.Drawing.Size(46, 21);
            this.comboUF.TabIndex = 11;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Location = new System.Drawing.Point(251, 88);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(28, 13);
            this.lblCEP.TabIndex = 12;
            this.lblCEP.Text = "CEP";
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Location = new System.Drawing.Point(270, 253);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(75, 23);
            this.btnCadastrar.TabIndex = 14;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Location = new System.Drawing.Point(20, 114);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(27, 13);
            this.lblCPF.TabIndex = 15;
            this.lblCPF.Text = "CPF";
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Location = new System.Drawing.Point(154, 114);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(23, 13);
            this.lblRG.TabIndex = 17;
            this.lblRG.Text = "RG";
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Location = new System.Drawing.Point(20, 204);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(49, 13);
            this.lblTelefone.TabIndex = 19;
            this.lblTelefone.Text = "Telefone";
            // 
            // lblSeparadorDados
            // 
            this.lblSeparadorDados.AutoSize = true;
            this.lblSeparadorDados.Location = new System.Drawing.Point(89, 17);
            this.lblSeparadorDados.Name = "lblSeparadorDados";
            this.lblSeparadorDados.Size = new System.Drawing.Size(265, 13);
            this.lblSeparadorDados.TabIndex = 20;
            this.lblSeparadorDados.Text = "ddddddddddddddddddddddddddddddddddddddddddd";
            this.lblSeparadorDados.Click += new System.EventHandler(this.lblSeparadorDados_Click);
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.Location = new System.Drawing.Point(7, 9);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(83, 13);
            this.lblDados.TabIndex = 21;
            this.lblDados.Text = "Dados Pessoais";
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Location = new System.Drawing.Point(7, 172);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(93, 13);
            this.lblContato.TabIndex = 23;
            this.lblContato.Text = "Dados de Contato";
            this.lblContato.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblSeparadorContato
            // 
            this.lblSeparadorContato.AutoSize = true;
            this.lblSeparadorContato.Location = new System.Drawing.Point(99, 180);
            this.lblSeparadorContato.Name = "lblSeparadorContato";
            this.lblSeparadorContato.Size = new System.Drawing.Size(253, 13);
            this.lblSeparadorContato.TabIndex = 22;
            this.lblSeparadorContato.Text = "ddddddddddddddddddddddddddddddddddddddddd";
            this.lblSeparadorContato.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Location = new System.Drawing.Point(172, 204);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(39, 13);
            this.lblCelular.TabIndex = 25;
            this.lblCelular.Text = "Celular";
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(20, 230);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(46, 13);
            this.lblWebsite.TabIndex = 27;
            this.lblWebsite.Text = "Website";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(180, 230);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 29;
            this.lblEmail.Text = "E-mail";
            // 
            // maskedCPF
            // 
            this.maskedCPF.Location = new System.Drawing.Point(53, 111);
            this.maskedCPF.Name = "maskedCPF";
            this.maskedCPF.Size = new System.Drawing.Size(95, 20);
            this.maskedCPF.TabIndex = 30;
            this.maskedCPF.Text = "XXX.XXX.XXX-XX";
            this.maskedCPF.Click += new System.EventHandler(this.maskedTextBox1_Click);
            // 
            // maskedRG
            // 
            this.maskedRG.Location = new System.Drawing.Point(183, 112);
            this.maskedRG.Name = "maskedRG";
            this.maskedRG.Size = new System.Drawing.Size(82, 20);
            this.maskedRG.TabIndex = 31;
            this.maskedRG.Text = "XX.XXX.XXX-X";
            this.maskedRG.Click += new System.EventHandler(this.maskedRG_Click);
            // 
            // maskedTelFixo
            // 
            this.maskedTelFixo.Location = new System.Drawing.Point(75, 201);
            this.maskedTelFixo.Mask = "(00) 0000-0000";
            this.maskedTelFixo.Name = "maskedTelFixo";
            this.maskedTelFixo.Size = new System.Drawing.Size(91, 20);
            this.maskedTelFixo.TabIndex = 32;
            this.maskedTelFixo.Click += new System.EventHandler(this.maskedTelFixo_Click);
            // 
            // maskedTelCel
            // 
            this.maskedTelCel.Location = new System.Drawing.Point(217, 201);
            this.maskedTelCel.Mask = "(99) 00000-0000";
            this.maskedTelCel.Name = "maskedTelCel";
            this.maskedTelCel.Size = new System.Drawing.Size(102, 20);
            this.maskedTelCel.TabIndex = 33;
            this.maskedTelCel.Click += new System.EventHandler(this.maskedTelCel_Click);
            // 
            // maskedWebsite
            // 
            this.maskedWebsite.Location = new System.Drawing.Point(72, 227);
            this.maskedWebsite.Name = "maskedWebsite";
            this.maskedWebsite.Size = new System.Drawing.Size(102, 20);
            this.maskedWebsite.TabIndex = 34;
            this.maskedWebsite.Text = "www.dominio.com";
            this.maskedWebsite.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedWebsite_MaskInputRejected);
            this.maskedWebsite.Click += new System.EventHandler(this.maskedWebsite_Click);
            // 
            // maskedEmail
            // 
            this.maskedEmail.Location = new System.Drawing.Point(221, 227);
            this.maskedEmail.Name = "maskedEmail";
            this.maskedEmail.Size = new System.Drawing.Size(124, 20);
            this.maskedEmail.TabIndex = 35;
            this.maskedEmail.Text = "usuario@dominio.com";
            this.maskedEmail.Click += new System.EventHandler(this.maskedEmail_Click);
            // 
            // maskedCEP
            // 
            this.maskedCEP.Location = new System.Drawing.Point(285, 86);
            this.maskedCEP.Mask = "00000-000";
            this.maskedCEP.Name = "maskedCEP";
            this.maskedCEP.Size = new System.Drawing.Size(69, 20);
            this.maskedCEP.TabIndex = 36;
            this.maskedCEP.Click += new System.EventHandler(this.maskedCEP_Click);
            // 
            // comboSexo
            // 
            this.comboSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSexo.FormattingEnabled = true;
            this.comboSexo.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboSexo.Location = new System.Drawing.Point(57, 137);
            this.comboSexo.Name = "comboSexo";
            this.comboSexo.Size = new System.Drawing.Size(91, 21);
            this.comboSexo.TabIndex = 38;
            this.comboSexo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(20, 140);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(31, 13);
            this.lblSexo.TabIndex = 37;
            this.lblSexo.Text = "Sexo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(162, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Nascimento";
            // 
            // maskedNascimento
            // 
            this.maskedNascimento.Location = new System.Drawing.Point(231, 137);
            this.maskedNascimento.Mask = "00/00/0000";
            this.maskedNascimento.Name = "maskedNascimento";
            this.maskedNascimento.Size = new System.Drawing.Size(96, 20);
            this.maskedNascimento.TabIndex = 40;
            this.maskedNascimento.Click += new System.EventHandler(this.maskedNascimento_Click);
            // 
            // maskedNumero
            // 
            this.maskedNumero.Location = new System.Drawing.Point(301, 59);
            this.maskedNumero.Mask = "0000";
            this.maskedNumero.Name = "maskedNumero";
            this.maskedNumero.Size = new System.Drawing.Size(38, 20);
            this.maskedNumero.TabIndex = 41;
            this.maskedNumero.Click += new System.EventHandler(this.maskedNumero_Click);
            // 
            // cadastroCliente
            // 
            this.ClientSize = new System.Drawing.Size(364, 289);
            this.Controls.Add(this.maskedNumero);
            this.Controls.Add(this.maskedNascimento);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboSexo);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.maskedCEP);
            this.Controls.Add(this.maskedEmail);
            this.Controls.Add(this.maskedWebsite);
            this.Controls.Add(this.maskedTelCel);
            this.Controls.Add(this.maskedTelFixo);
            this.Controls.Add(this.maskedRG);
            this.Controls.Add(this.maskedCPF);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblWebsite);
            this.Controls.Add(this.lblCelular);
            this.Controls.Add(this.lblContato);
            this.Controls.Add(this.lblSeparadorContato);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.lblSeparadorDados);
            this.Controls.Add(this.lblTelefone);
            this.Controls.Add(this.lblRG);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.btnCadastrar);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.comboUF);
            this.Controls.Add(this.lblUF);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.txtSobrenome);
            this.Controls.Add(this.lblSobrenome);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.lblNome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cadastroCliente";
            this.Text = "Cadastro de Clientes";
            this.Load += new System.EventHandler(this.cadastroCliente_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtSobrenome;
        private System.Windows.Forms.Label lblSobrenome;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.ComboBox comboUF;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Label lblSeparadorDados;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Label lblContato;
        private System.Windows.Forms.Label lblSeparadorContato;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.MaskedTextBox maskedCPF;
        private System.Windows.Forms.MaskedTextBox maskedRG;
        private System.Windows.Forms.MaskedTextBox maskedTelFixo;
        private System.Windows.Forms.MaskedTextBox maskedTelCel;
        private System.Windows.Forms.MaskedTextBox maskedWebsite;
        private System.Windows.Forms.MaskedTextBox maskedEmail;
        private System.Windows.Forms.MaskedTextBox maskedCEP;
        private System.Windows.Forms.ComboBox comboSexo;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox maskedNascimento;
        private System.Windows.Forms.MaskedTextBox maskedNumero;


    }
}