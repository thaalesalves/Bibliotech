﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class mostraPerfil : Form {
        /* Construtor */
        public mostraPerfil() {
            InitializeComponent();

            /* Separadores */
            lblSeparadorDados.AutoSize = false; // Separador de Dados Pessoais
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;

            lblSeparadorContato.AutoSize = false; // Separador de Dados de Contato
            lblSeparadorContato.Height = 2;
            lblSeparadorContato.BorderStyle = BorderStyle.Fixed3D;
        }

        // dao.getNome(idConvert), dao.getSobrenome(idConvert), dao.getRG(idConvert), dao.getCPF(idConvert), 
        // dao.getEndereco(idConvert), dao.getCEP(idConvert), dao.getCidade(idConvert), dao.getUF(idConvert), 
        // dao.getTelFixo(idConvert), dao.getTelCel(idConvert), dao.getSite(idConvert), dao.getEmail(idConvert), 
        // dao.getNumero(idConvert)

        /* Métodos */
        public void setCampos(string nome, string sobrenome, string rg, string cpf, string endereco, string cep, 
            string cidade, string uf, string fixo, string celular, string site, string email, string numero, string sex, string nasc) {
            this.Text = "Dados de " + nome;
            lblNome.Text = String.Format("Nome: " + nome + " " + sobrenome);
            lblCPF.Text = String.Format("CPF: " + cpf);
            lblRG.Text = String.Format("RG: " + rg);
            lblEndereco.Text = String.Format("Endereço: " + endereco + ", " + numero + " - " + cidade + ", " + uf);
            lblFixo.Text = "Telefone Fixo: " + fixo;
            lblTelCel.Text = "Telefone Celular: " + celular;
            lblWebsite.Text = "Website: " + site;
            lblEmail.Text = "E-mail: " + email;
            lblNasc.Text = "Data de Nascimento: " + nasc;
            lblSexo.Text = "Sexo: " + sex;
            lblCEP.Text = "CEP: " + cep;
        }

        /* Forms */
        private void mostraPerfil_Load(object sender, EventArgs e) {
            MaximizeBox = false;
            MinimizeBox = false;
        }
    }
}
