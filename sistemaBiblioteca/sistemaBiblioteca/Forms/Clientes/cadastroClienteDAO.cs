﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace sistemaBiblioteca {
    class cadastroClienteDAO {
        /* Instanciação de Objetos */
        NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);
        Metodos mtd = new Metodos(); // Classe de métodos

        /* Declaração de Variáveis */
        static string connString = null;

        /* Construtor */
        public cadastroClienteDAO() {
            connString = String.Format(
                "Server=127.0.0.1;" + // Servidor
                "Port=5432;" + // Porta
                "User Id=bibliotech;" + // Usuário 
                "Password=bibliotech;" +  // Senha
                "Database=bibliotech;" // Base de dados
            );
        }    

        /* Métodos de Usuário */        
        public void cadastraUsuario(string nome, string sobrenome, string endereco, int numero, string cpf, string rg, string uf, string cep, string cidade, string fixo, string celular, string email, string website, string sexo, string nasc) { // Cadastra usuários no banco
            try {
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString)) {
                    pgsqlConnection.Open();
                    string cmdInserir = String.Format("INSERT INTO usuario VALUES(DEFAULT, '" + nome + "','" + sobrenome + "','" + nasc + "','" + sexo + "','" + cpf + "','" + rg + "','" + endereco + "'," + numero + ",'" + cep + "','" + cidade + "','" + uf + "','" + fixo + "','" + celular + "','" + email + "','" + website + "');");
                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, pgsqlConnection)) {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }

            catch (NpgsqlException ex) {
                throw ex;
            }

            catch (Exception ex) {
                throw ex;
            }

            finally {
               pgsqlConnection.Close();
            }
        }        
    }
}
