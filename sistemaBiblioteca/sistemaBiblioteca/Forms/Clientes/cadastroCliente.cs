﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace sistemaBiblioteca {
    public partial class cadastroCliente : Form {
        /* Construtor */
        public cadastroCliente() {
            InitializeComponent();

            /* Separadores */
            lblSeparadorDados.AutoSize = false; // Separador de Dados Pessoais
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;

            lblSeparadorContato.AutoSize = false; // Separador de Dados de Contato
            lblSeparadorContato.Height = 2;
            lblSeparadorContato.BorderStyle = BorderStyle.Fixed3D;

            /* Máscaras */
            maskedRG.Mask = "00.000.000-0";
            maskedRG.Text = "__.___.___-_";

            maskedCPF.Mask = "000.000.000-00";
            maskedCPF.Text = "___.___.___-__";

            maskedCEP.Mask = "00000-000";
            maskedCEP.Text = "_____-___";
        }

        /* Instanciação de Objetos */
        cadastroClienteDAO dao = new cadastroClienteDAO(); // objeto de acesso ao banco de dados
        Metodos mtd = new Metodos(); // Objeto da superclasse de métodos

        /* Declação de Variáveis */
        private string nome, sobrenome, endereco, cpf, rg, uf, cidade, cep;
        private string fixo, celular, website, email, nasc, sexo;
        private int numero;

        /* Forms */
        private void cadastroCliente_Load(object sender, EventArgs e) {
            MaximizeBox = false;
            MinimizeBox = false;            
        }

        private void txtNome_Click(object sender, EventArgs e) {
            txtNome.Text = string.Empty;
        }

        private void txtSobrenome_Click(object sender, EventArgs e) {
            txtSobrenome.Text = string.Empty;            
        }

        private void lblEndereco_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtEndereco_Click(object sender, EventArgs e) {
            txtEndereco.Text = string.Empty;
        }

        private void txtCidade_Click(object sender, EventArgs e) {
            txtCidade.Text = string.Empty;
        }

        private void txtNumero_Click(object sender, EventArgs e) {
            
        }

        private void maskedNumero_Click(object sender, EventArgs e) {
            maskedNumero.Text = String.Empty;
        }

        private void btnCadastrar_Click(object sender, EventArgs e) {
            try {
                if (maskedWebsite.Text == string.Empty || maskedWebsite.Text == null || maskedWebsite.Text == "www.dominio.com") {
                    website = "nenhum";
                } else {
                    website = maskedWebsite.Text;
                }

                if (maskedEmail.Text == string.Empty || maskedEmail.Text == null || maskedEmail.Text == "usuario@dominio.com") {
                    email = "nenhum";
                } else {
                    email = maskedEmail.Text;
                }

                celular = maskedTelCel.Text;
                fixo = maskedTelFixo.Text;
                nasc = maskedNascimento.Text;
                nome = mtd.fixEncoding(txtNome.Text);
                endereco = mtd.fixEncoding(txtEndereco.Text);
                sobrenome = mtd.fixEncoding(txtSobrenome.Text);
                cidade = mtd.fixEncoding(txtCidade.Text);
                numero = Convert.ToInt32(maskedNumero.Text);
                cpf = maskedCPF.Text;
                rg = maskedRG.Text;
                uf = comboUF.Text;
                cep = maskedCEP.Text;
                sexo = comboSexo.Text;
                dao.cadastraUsuario(nome, sobrenome, endereco, numero, cpf, rg, uf, cep, cidade, fixo, celular, email, website, sexo, nasc);
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }

            finally {
                MessageBox.Show("Usuário " + nome + " " + sobrenome + " cadastrado com sucesso.", "Aviso");
            }
        }

        private void txtCPF_Click(object sender, EventArgs e) {
            //txtCPF.Text = string.Empty;
        }

        private void txtRG_TextChanged(object sender, EventArgs e) {
            //txtCPF.Text = string.Empty;
        }

        private void lblSeparadorDados_Click(object sender, EventArgs e) {
            lblSeparadorDados.AutoSize = false;
            lblSeparadorDados.Height = 2;
            lblSeparadorDados.BorderStyle = BorderStyle.Fixed3D;
        }

        private void label2_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void label3_Click(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtRG_Click(object sender, EventArgs e) {
            //txtRG.Text = string.Empty;
        }

        private void txtTelFixo_TextChanged(object sender, EventArgs e) {
            // Deixar em branco
        }

        private void txtTelFixo_Click(object sender, EventArgs e) {
            //txtTelFixo.Text = string.Empty;
        }

        private void txtTelCel_Click(object sender, EventArgs e) {
            //txtTelCel.Text = string.Empty;
        }

        private void txtWebsite_Click(object sender, EventArgs e) {
            //txtWebsite.Text = string.Empty;
        }

        private void txtEmail_Click(object sender, EventArgs e) {
            //txtEmail.Text = string.Empty;
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e) {

        }

        private void maskedTextBox1_Click(object sender, EventArgs e) {
            maskedCPF.Text = string.Empty;
        }

        private void maskedRG_Click(object sender, EventArgs e) {
            maskedRG.Text = string.Empty;
        }

        private void maskedTelCel_Click(object sender, EventArgs e) {
            maskedTelCel.Text = string.Empty;
        }

        private void maskedTelFixo_Click(object sender, EventArgs e) {
            maskedTelFixo.Text = string.Empty;
        }

        private void maskedEmail_Click(object sender, EventArgs e) {
            maskedEmail.Text = string.Empty;
        }

        private void maskedWebsite_MaskInputRejected(object sender, MaskInputRejectedEventArgs e) {
            maskedWebsite.Text = string.Empty;
        }

        private void maskedCEP_Click(object sender, EventArgs e) {
            maskedCEP.Text = string.Empty;
        }

        private void maskedWebsite_Click(object sender, EventArgs e) {
            maskedWebsite.Text = string.Empty;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void maskedNascimento_Click(object sender, EventArgs e) {
            maskedNascimento.Text = string.Empty;
        }             
    }
}
