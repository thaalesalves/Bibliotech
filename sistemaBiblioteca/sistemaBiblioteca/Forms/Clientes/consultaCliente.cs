﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sistemaBiblioteca {
    public partial class consultaCliente : Form {
        public consultaCliente() {
            InitializeComponent();
        }
         /* Instanciação de Objetos */
        consultaClienteDAO dao = new consultaClienteDAO();
        Metodos mtd = new Metodos();
        atualizaCliente atualizaCliente = new atualizaCliente();
        mostraPerfil mostraPerfil = new mostraPerfil();

        /* Variáveis */
        private int idConvert;

        /* Métodos */
        private void atualizarExibicao() { // Método de atualizar a data grid view com todos os elementos
            dgvTesteExibicao.DataSource = dao.getRegistro(); 
        }

        private void atualizarExibicaoID() { // Método de atualizar a data grid view com os elementos específicos
            dgvTesteExibicao.DataSource = dao.getRegistroID(txtBusca.Text);
        }

        /* Forms */
        private void btnExibir_Click(object sender, EventArgs e)
        {
            try {
                atualizarExibicao();
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }        

        private void dgvTesteExibicao_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            idConvert = Convert.ToInt32(dgvTesteExibicao.Rows[e.RowIndex].Cells[0].Value.ToString());
            atualizaCliente.Id = idConvert;
        }

        private void consultaCliente_Load(object sender, EventArgs e) {
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void textBox1_Click(object sender, EventArgs e) {
            txtBusca.Text = string.Empty;
        }

        private void txtBusca_TextChanged(object sender, EventArgs e) {

        }

        private void btnBusca_Click(object sender, EventArgs e) {            
            try {
                if (txtBusca.Text == string.Empty || txtBusca.Text == "Buscar") {
                    MessageBox.Show("Erro: o campo de busca não pode estar em branco.", "Erro");
                } else {
                    dao.getRegistroID(txtBusca.Text);
                    atualizarExibicaoID();
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void btnBuscarNome_Click(object sender, EventArgs e) {
            dao.getRegistroNome(Convert.ToString(txtBusca.Text));

            try {
                atualizarExibicaoID();
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.Message, "Erro");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e) {
            try {
                if (idConvert == 0) {
                    MessageBox.Show("Erro: nenhum ID de usuário foi selecionado.", "Erro");
                } else {
                    atualizaCliente.setCampos(dao.getNome(idConvert), dao.getSobrenome(idConvert), dao.getRG(idConvert), dao.getCPF(idConvert), dao.getEndereco(idConvert), dao.getCEP(idConvert), dao.getCidade(idConvert), dao.getUF(idConvert), dao.getTelFixo(idConvert), dao.getTelCel(idConvert), dao.getSite(idConvert), dao.getEmail(idConvert), dao.getNumero(idConvert), dao.getSexo(idConvert), dao.getNasc(idConvert));
                    atualizaCliente.ShowDialog();
                    atualizarExibicao();
                }                
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }

        private void btnRemover_Click(object sender, EventArgs e) {
            try {
                if (idConvert == 0) {
                    MessageBox.Show("Erro: nenhum ID de usuário foi selecionado.", "Erro");
                } else {
                    dao.deletaRegistro(idConvert);
                    MessageBox.Show("Registro " + idConvert + " removido com sucesso.", "Aviso");
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }

            finally {
                atualizarExibicao();
            }
        }

        private void btnVer_Click(object sender, EventArgs e) {
            //exibePerfil.ShowDialog();
        }

        private void btnDados_Click(object sender, EventArgs e) {
            try {
                if (idConvert == 0) {
                    MessageBox.Show("Erro: nenhum ID de usuário foi selecionado.", "Erro");
                } else {
                    mostraPerfil.setCampos(dao.getNome(idConvert), dao.getSobrenome(idConvert), dao.getRG(idConvert), dao.getCPF(idConvert), dao.getEndereco(idConvert), dao.getCEP(idConvert), dao.getCidade(idConvert), dao.getUF(idConvert), dao.getTelFixo(idConvert), dao.getTelCel(idConvert), dao.getSite(idConvert), dao.getEmail(idConvert), dao.getNumero(idConvert), dao.getSexo(idConvert), dao.getNasc(idConvert));
                    mostraPerfil.ShowDialog();
                }
            }

            catch (Exception ex) {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }
    }
}
