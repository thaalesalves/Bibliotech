﻿namespace sistemaBiblioteca {
    partial class mostraPerfil {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mostraPerfil));
            this.lblNome = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblRG = new System.Windows.Forms.Label();
            this.lblFixo = new System.Windows.Forms.Label();
            this.lblTelCel = new System.Windows.Forms.Label();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.lblSeparadorDados = new System.Windows.Forms.Label();
            this.lblContato = new System.Windows.Forms.Label();
            this.lblSeparadorContato = new System.Windows.Forms.Label();
            this.lblNasc = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblCEP = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(21, 29);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(45, 13);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "lblNome";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Location = new System.Drawing.Point(21, 65);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(63, 13);
            this.lblEndereco.TabIndex = 1;
            this.lblEndereco.Text = "lblEndereco";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Location = new System.Drawing.Point(21, 47);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(37, 13);
            this.lblCPF.TabIndex = 2;
            this.lblCPF.Text = "lblCPF";
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Location = new System.Drawing.Point(372, 47);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(33, 13);
            this.lblRG.TabIndex = 3;
            this.lblRG.Text = "lblRG";
            // 
            // lblFixo
            // 
            this.lblFixo.AutoSize = true;
            this.lblFixo.Location = new System.Drawing.Point(12, 149);
            this.lblFixo.Name = "lblFixo";
            this.lblFixo.Size = new System.Drawing.Size(36, 13);
            this.lblFixo.TabIndex = 4;
            this.lblFixo.Text = "lblFixo";
            // 
            // lblTelCel
            // 
            this.lblTelCel.AutoSize = true;
            this.lblTelCel.Location = new System.Drawing.Point(173, 149);
            this.lblTelCel.Name = "lblTelCel";
            this.lblTelCel.Size = new System.Drawing.Size(49, 13);
            this.lblTelCel.TabIndex = 5;
            this.lblTelCel.Text = "lblCelular";
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(12, 167);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(56, 13);
            this.lblWebsite.TabIndex = 6;
            this.lblWebsite.Text = "lblWebsite";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(173, 167);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(42, 13);
            this.lblEmail.TabIndex = 7;
            this.lblEmail.Text = "lblEmail";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.Location = new System.Drawing.Point(7, 10);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(83, 13);
            this.lblDados.TabIndex = 23;
            this.lblDados.Text = "Dados Pessoais";
            // 
            // lblSeparadorDados
            // 
            this.lblSeparadorDados.AutoSize = true;
            this.lblSeparadorDados.Location = new System.Drawing.Point(89, 18);
            this.lblSeparadorDados.Name = "lblSeparadorDados";
            this.lblSeparadorDados.Size = new System.Drawing.Size(469, 13);
            this.lblSeparadorDados.TabIndex = 22;
            this.lblSeparadorDados.Text = "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Location = new System.Drawing.Point(7, 128);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(93, 13);
            this.lblContato.TabIndex = 25;
            this.lblContato.Text = "Dados de Contato";
            // 
            // lblSeparadorContato
            // 
            this.lblSeparadorContato.AutoSize = true;
            this.lblSeparadorContato.Location = new System.Drawing.Point(101, 136);
            this.lblSeparadorContato.Name = "lblSeparadorContato";
            this.lblSeparadorContato.Size = new System.Drawing.Size(457, 13);
            this.lblSeparadorContato.TabIndex = 24;
            this.lblSeparadorContato.Text = "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
            // 
            // lblNasc
            // 
            this.lblNasc.AutoSize = true;
            this.lblNasc.Location = new System.Drawing.Point(372, 29);
            this.lblNasc.Name = "lblNasc";
            this.lblNasc.Size = new System.Drawing.Size(176, 13);
            this.lblNasc.TabIndex = 26;
            this.lblNasc.Text = "Data de Nascimento: XX/XX/XXXX";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(372, 65);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(41, 13);
            this.lblSexo.TabIndex = 27;
            this.lblSexo.Text = "lblSexo";
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Location = new System.Drawing.Point(21, 83);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(38, 13);
            this.lblCEP.TabIndex = 28;
            this.lblCEP.Text = "lblCEP";
            // 
            // mostraPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 189);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.lblNasc);
            this.Controls.Add(this.lblContato);
            this.Controls.Add(this.lblSeparadorContato);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.lblSeparadorDados);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblWebsite);
            this.Controls.Add(this.lblTelCel);
            this.Controls.Add(this.lblFixo);
            this.Controls.Add(this.lblRG);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.lblNome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mostraPerfil";
            this.Text = "Dados";
            this.Load += new System.EventHandler(this.mostraPerfil_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.Label lblFixo;
        private System.Windows.Forms.Label lblTelCel;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Label lblSeparadorDados;
        private System.Windows.Forms.Label lblContato;
        private System.Windows.Forms.Label lblSeparadorContato;
        private System.Windows.Forms.Label lblNasc;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblCEP;
    }
}