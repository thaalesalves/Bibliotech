﻿// PROJETO DE AMBIENTES VISUAIS //
// BIBLIOTECH, SISTEMA DE GESTÃO BIBLIOTECÁRIA //
// DESENVOLVIDO POR THALES, GILBERTO E LINCON //
// TURMA DE SISTEMAS DE INFORMAÇÃO, 3º SEMESTRE, TURMA A //
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using Npgsql;

namespace sistemaBiblioteca {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMainProgram());
        }
    }
}
